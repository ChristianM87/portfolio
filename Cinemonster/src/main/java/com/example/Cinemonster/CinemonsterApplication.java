package com.example.Cinemonster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemonsterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinemonsterApplication.class, args);
	}

}
