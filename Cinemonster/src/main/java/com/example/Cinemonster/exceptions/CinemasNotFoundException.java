package com.example.Cinemonster.exceptions;

public class CinemasNotFoundException extends RuntimeException{

    public CinemasNotFoundException(String message){
        super(message);
    }
}
