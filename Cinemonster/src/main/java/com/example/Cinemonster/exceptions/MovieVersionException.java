package com.example.Cinemonster.exceptions;

public class MovieVersionException extends RuntimeException{
    public MovieVersionException(String message){
        super(message);
    }
}
