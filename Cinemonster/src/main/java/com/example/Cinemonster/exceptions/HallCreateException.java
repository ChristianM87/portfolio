package com.example.Cinemonster.exceptions;

public class HallCreateException extends RuntimeException{

    public HallCreateException(String message){
        super(message);
    }
}
