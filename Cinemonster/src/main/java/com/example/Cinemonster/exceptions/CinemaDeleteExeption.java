package com.example.Cinemonster.exceptions;

public class CinemaDeleteExeption extends RuntimeException{

    public CinemaDeleteExeption(String message){
        super(message);
    }
}
