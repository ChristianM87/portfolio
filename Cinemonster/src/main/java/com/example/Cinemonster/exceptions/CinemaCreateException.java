package com.example.Cinemonster.exceptions;

import com.example.Cinemonster.enums.ExceptionReasons;

public class CinemaCreateException extends RuntimeException {

    private ExceptionReasons reason;

    public CinemaCreateException(String message, ExceptionReasons reason) {
        super(message);
        this.reason = reason;
    }

    public ExceptionReasons getReason() {
        return reason;
    }
}
