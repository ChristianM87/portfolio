package com.example.Cinemonster.exceptions;

public class HallVersionException extends RuntimeException{

    public HallVersionException(String message){
        super(message);
    }
}
