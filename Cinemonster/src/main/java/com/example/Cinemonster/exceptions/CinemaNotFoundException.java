package com.example.Cinemonster.exceptions;

import com.example.Cinemonster.enums.ExceptionReasons;

public class CinemaNotFoundException extends RuntimeException{
    private ExceptionReasons reason;

    public CinemaNotFoundException(String message, ExceptionReasons reason) {
        super(message);
        this.reason = reason;
    }

    public ExceptionReasons getReason() {
        return reason;
    }
}
