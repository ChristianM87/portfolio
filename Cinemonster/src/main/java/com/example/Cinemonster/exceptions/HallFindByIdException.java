package com.example.Cinemonster.exceptions;

public class HallFindByIdException extends RuntimeException{
    public HallFindByIdException(String message){
        super(message);
    }
}
