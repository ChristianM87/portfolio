package com.example.Cinemonster.exceptionHandler;

import com.example.Cinemonster.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ProblemDetail cinemaCreateException(CinemaCreateException e){
        ProblemDetail problem = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
        problem.setTitle("Falsche Angaben");
        problem.setDetail(e.getMessage());
        problem.setProperty("reason", e.getReason());
        return problem;
    }

    @ExceptionHandler
    public ProblemDetail cinemaNotFoundException(CinemaNotFoundException e){
        ProblemDetail problem = ProblemDetail.forStatus(HttpStatus.NOT_FOUND);
        problem.setTitle("Cinema mit dieser Id nicht gefunden");
        problem.setDetail(e.getMessage());
        problem.setProperty("reason", e.getReason());
        return problem;
    }

    @ExceptionHandler
    public ProblemDetail hallCreateException(HallCreateException e){
        ProblemDetail problem = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
        problem.setTitle("Falsche Angaben");
        problem.setDetail(e.getMessage());
        return problem;
    }

    @ExceptionHandler
    public ProblemDetail hallFindByIdException(HallFindByIdException e){
        ProblemDetail problem = ProblemDetail.forStatus(HttpStatus.NOT_FOUND);
        problem.setTitle("Saal mit dieser ID nicht gefunden");
        problem.setDetail(e.getMessage());
        return problem;
    }

    @ExceptionHandler
    public ProblemDetail movieVersionException(MovieVersionException e){
        ProblemDetail problem = ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
        problem.setTitle("MovieVersion darf nur von 5D auf 3D geändert werden");
        problem.setDetail(e.getMessage());
        return problem;
    }

}
