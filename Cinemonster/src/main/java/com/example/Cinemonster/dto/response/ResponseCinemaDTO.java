package com.example.Cinemonster.dto.response;

import lombok.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ResponseCinemaDTO {
    private String name;
    private String address;
    private String manager;
    private int maxHalls;

    private List<Integer> getCinemaHalls;
}
