package com.example.Cinemonster.dto.request;

import com.example.Cinemonster.enums.MovieVersion;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RequestMovieDTO {


    private int movieId;
    @NotBlank(message = "Es muss ein Titel angegeben werden!")
    private String title;
    @NotBlank(message = "Es muss ein Hauptcharakter angegeben werden")
    private String mainCharacter;
    @NotBlank(message = "Bitte eine Kurzbeschreibung des Films hinzufügen")
    private String description;
    private String premieredAt;
    @NotBlank(message = "Filmversion fehlt!")
    private MovieVersion movieVersion;
    private int hallId;
}
