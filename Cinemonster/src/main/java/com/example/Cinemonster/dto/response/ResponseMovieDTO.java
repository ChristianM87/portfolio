package com.example.Cinemonster.dto.response;

import com.example.Cinemonster.enums.MovieVersion;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ResponseMovieDTO {
    private int movieId;
    private String title;
    private String mainCharacter;
    private String description;
    private String premieredAt;
    private MovieVersion movieVersion;
}
