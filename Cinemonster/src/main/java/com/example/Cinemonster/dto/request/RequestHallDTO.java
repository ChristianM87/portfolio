package com.example.Cinemonster.dto.request;

import com.example.Cinemonster.enums.MovieVersion;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class RequestHallDTO {

    private int hallId;
    @Min(1)
    private int capacity;
    @PositiveOrZero
    private int occupiedSeats;
    @NotBlank(message = "supportedMovieVersion can not be blank")
    private MovieVersion supportedMovieVersion;
    private int cinemaId;
}
