package com.example.Cinemonster.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RequestCinemaDTO {
    private String name;
    private String address;
    private String manager;
    private int maxHalls;
}
