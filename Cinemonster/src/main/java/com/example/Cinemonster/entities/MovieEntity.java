package com.example.Cinemonster.entities;

import com.example.Cinemonster.enums.MovieVersion;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Movies")
public class MovieEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int movieId;
    private String title;
    private String mainCharacter;
    private String description;
    private String premieredAt;
    @Enumerated(EnumType.STRING)
    private MovieVersion movieVersion;

    @ManyToOne
    @JoinColumn(name = "hallId")
    private HallEntity hallEntity;



}
