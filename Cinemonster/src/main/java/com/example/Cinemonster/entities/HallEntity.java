package com.example.Cinemonster.entities;

import com.example.Cinemonster.enums.MovieVersion;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity

public class HallEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int hallId;
    private int capacity;
    private int occupiedSeats;
    @Enumerated(EnumType.STRING)
    private MovieVersion supportedMovieVersion;

    @ManyToOne
    @JoinColumn(name = "cinemaId")
    private CinemaEntity cinemaEntity;

    @OneToMany(mappedBy = "hallEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MovieEntity> movies;
}
