package com.example.Cinemonster.entities;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Cinemas")

public class CinemaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cinemaId;
    private String name;
    private String address;
    private String manager;
    private int maxHalls;

    @OneToMany(mappedBy = "cinemaEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HallEntity> halls;
}
