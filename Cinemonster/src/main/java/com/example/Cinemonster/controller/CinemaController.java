package com.example.Cinemonster.controller;

import com.example.Cinemonster.dto.request.RequestCinemaDTO;
import com.example.Cinemonster.dto.response.ResponseCinemaDTO;
import com.example.Cinemonster.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("api/cinema")
public class CinemaController {

    @Autowired
    CinemaService cinemaService;

    @PostMapping
    public ResponseEntity<ResponseCinemaDTO> createCinema(@RequestBody RequestCinemaDTO requestCinemaDTO) {
        return new ResponseEntity<>(cinemaService.createCinema(requestCinemaDTO), HttpStatus.CREATED);
    }

    @GetMapping("{cinemaId}")
    public ResponseEntity<ResponseCinemaDTO> findCinemaById(@PathVariable int cinemaId) {
        return new ResponseEntity<>(cinemaService.findCinemaById(cinemaId), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ResponseCinemaDTO>> getAllCinemas() {
            return new ResponseEntity<>(cinemaService.getAllCinemas(), HttpStatus.OK);
        }


    @DeleteMapping("{cinemaId}")
    public ResponseEntity<String> deleteCinema(@PathVariable int cinemaId) {
        return new ResponseEntity<>(cinemaService.deleteCinema(cinemaId), HttpStatus.OK);
    }
}
