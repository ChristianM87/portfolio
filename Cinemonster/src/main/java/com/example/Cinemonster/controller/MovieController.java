package com.example.Cinemonster.controller;

import com.example.Cinemonster.dto.request.RequestMovieDTO;
import com.example.Cinemonster.dto.response.ResponseMovieDTO;
import com.example.Cinemonster.enums.MovieVersion;
import com.example.Cinemonster.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/movie")
public class MovieController {

    @Autowired
    private MovieService movieService;

    @PostMapping
    public ResponseEntity<ResponseMovieDTO> createMovie(@RequestBody RequestMovieDTO requestMovieDTO) {
        return new ResponseEntity<>(movieService.createMovie(requestMovieDTO), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<ResponseMovieDTO>> showMovies() {
        List<ResponseMovieDTO> movies = movieService.showMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("{movieId}")
    private ResponseEntity<ResponseMovieDTO> updateMovie(@PathVariable int movieId, @RequestBody RequestMovieDTO requestMovieDTO) {
        return new ResponseEntity<>(movieService.updateMovie(movieId, requestMovieDTO), HttpStatus.OK);
    }

    @GetMapping("{movieVersion}")
    public ResponseEntity<List<ResponseMovieDTO>> getMoviesByVersion(@PathVariable MovieVersion movieVersion) {
        List<ResponseMovieDTO> movies = movieService.getMoviesByVersion(movieVersion);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
}

