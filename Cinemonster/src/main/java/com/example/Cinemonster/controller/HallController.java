package com.example.Cinemonster.controller;

import com.example.Cinemonster.dto.request.RequestHallDTO;
import com.example.Cinemonster.dto.response.ResponseHallDTO;
import com.example.Cinemonster.service.HallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/hall")

public class HallController {

    @Autowired
    private HallService hallService;

    @PostMapping
    public ResponseEntity<ResponseHallDTO> createHall(@RequestBody RequestHallDTO requestHallDTO){
        return new ResponseEntity<>(hallService.createHall(requestHallDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{hallId}")
    public ResponseEntity<ResponseHallDTO> updateHall(@PathVariable int hallId, @RequestBody RequestHallDTO requestHallDTO){
        return new ResponseEntity<>(hallService.updateHall(hallId, requestHallDTO), HttpStatus.ACCEPTED);
    }

    @GetMapping("/{hallId}")
    public ResponseEntity<ResponseHallDTO> showHall(@PathVariable int hallId){
        return new ResponseEntity<>(hallService.showHall(hallId), HttpStatus.OK);
    }
}
