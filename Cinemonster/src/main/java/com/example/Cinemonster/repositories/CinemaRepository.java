package com.example.Cinemonster.repositories;

import com.example.Cinemonster.entities.CinemaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CinemaRepository extends JpaRepository<CinemaEntity, Integer> {
}
