package com.example.Cinemonster.repositories;

import com.example.Cinemonster.entities.MovieEntity;
import com.example.Cinemonster.enums.MovieVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<MovieEntity, Integer> {

    List<MovieEntity> findByMovieVersion(MovieVersion movieVersion);

}
