package com.example.Cinemonster.repositories;

import com.example.Cinemonster.entities.CinemaEntity;
import com.example.Cinemonster.entities.HallEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HallRepository extends JpaRepository<HallEntity, Integer> {
    int countByCinemaEntity(CinemaEntity cinemaEntity);

}
