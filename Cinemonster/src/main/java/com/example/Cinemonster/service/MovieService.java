package com.example.Cinemonster.service;

import com.example.Cinemonster.dto.request.RequestMovieDTO;
import com.example.Cinemonster.dto.response.ResponseMovieDTO;
import com.example.Cinemonster.entities.HallEntity;
import com.example.Cinemonster.entities.MovieEntity;
import com.example.Cinemonster.enums.MovieVersion;
import com.example.Cinemonster.exceptions.HallVersionException;
import com.example.Cinemonster.repositories.HallRepository;
import com.example.Cinemonster.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private HallRepository hallRepository;

    public ResponseMovieDTO createMovie(RequestMovieDTO requestMovieDTO) {
        Optional<HallEntity> hallEntityOptional = hallRepository.findById(requestMovieDTO.getHallId());
        if (hallEntityOptional.isEmpty()) {
            throw new RuntimeException("Keine entsprechende Halle gefunden mit der ID: " + requestMovieDTO.getHallId());
        }
        HallEntity hallEntity = hallEntityOptional.get();
        if (!hallEntity.getSupportedMovieVersion().equals(requestMovieDTO.getMovieVersion())) {
            throw new HallVersionException("Die angegebene Filmversion wird vom Saal nicht unterstützt!");
        }
        MovieEntity movie = new MovieEntity();
        movie.setTitle(requestMovieDTO.getTitle());
        movie.setMainCharacter(requestMovieDTO.getMainCharacter());
        movie.setDescription(requestMovieDTO.getDescription());
        movie.setPremieredAt(requestMovieDTO.getPremieredAt());
        movie.setMovieVersion(requestMovieDTO.getMovieVersion());
        movie.setHallEntity(hallEntity);
        movieRepository.save(movie);
        return new ResponseMovieDTO(movie.getMovieId(), movie.getTitle(), movie.getMainCharacter(), movie.getDescription(), movie.getPremieredAt(), movie.getMovieVersion()
        );
    }


    public List<ResponseMovieDTO> showMovies() {
        List<MovieEntity> movies = movieRepository.findAll();
        List<ResponseMovieDTO> responseMovieDTOS = new ArrayList<>();
        for (MovieEntity movie : movies) {
            responseMovieDTOS.add(convertToDTO(movie));
        }
        return responseMovieDTOS;
    }

    public ResponseMovieDTO convertToDTO(MovieEntity movieEntity) {
        ResponseMovieDTO responseMovieDTO = new ResponseMovieDTO();
        responseMovieDTO.setTitle(movieEntity.getTitle());
        responseMovieDTO.setMainCharacter(movieEntity.getMainCharacter());
        responseMovieDTO.setDescription(movieEntity.getDescription());
        responseMovieDTO.setPremieredAt(movieEntity.getPremieredAt());
        responseMovieDTO.setMovieVersion(movieEntity.getMovieVersion());
        responseMovieDTO.setMovieId(movieEntity.getMovieId());
        return responseMovieDTO;
    }

    public ResponseMovieDTO updateMovie(int movieId, RequestMovieDTO requestMovieDTO) {
        Optional<MovieEntity> movieOptional = movieRepository.findById(movieId);
        MovieEntity movie = movieOptional.get();
        movie.setMovieId(movieId);
        movie.setTitle(requestMovieDTO.getTitle());
        movie.setMainCharacter(requestMovieDTO.getMainCharacter());
        movie.setDescription(requestMovieDTO.getDescription());
        movie.setPremieredAt(requestMovieDTO.getPremieredAt());
        movie.setMovieVersion(requestMovieDTO.getMovieVersion());
        movieRepository.deleteById(movieId);
        movieRepository.save(movie);
        return new ResponseMovieDTO(movie.getMovieId(), movie.getTitle(), movie.getMainCharacter(), movie.getDescription(), movie.getPremieredAt(), movie.getMovieVersion());
    }



    public List<ResponseMovieDTO> getMoviesByVersion(MovieVersion movieVersion) {
        List<MovieEntity> movies = movieRepository.findByMovieVersion(movieVersion);
        List<ResponseMovieDTO> responseMovieDTOs = new ArrayList<>();
        for (MovieEntity movie : movies) {
            if (movie.getMovieVersion() == movieVersion) {
                ResponseMovieDTO responseMovieDTO = new ResponseMovieDTO();
                responseMovieDTO.setMovieId(movie.getMovieId());
                responseMovieDTO.setTitle(movie.getTitle());
                responseMovieDTO.setMainCharacter(movie.getMainCharacter());
                responseMovieDTO.setDescription(movie.getDescription());
                responseMovieDTO.setPremieredAt(movie.getPremieredAt());
                responseMovieDTO.setMovieVersion(movie.getMovieVersion());
                responseMovieDTOs.add(responseMovieDTO);
            }
        }
        return responseMovieDTOs;
    }

}


