package com.example.Cinemonster.service;

import com.example.Cinemonster.dto.request.RequestCinemaDTO;
import com.example.Cinemonster.dto.response.ResponseCinemaDTO;
import com.example.Cinemonster.entities.CinemaEntity;
import com.example.Cinemonster.entities.HallEntity;
import com.example.Cinemonster.entities.MovieEntity;
import com.example.Cinemonster.exceptions.CinemaCreateException;
import com.example.Cinemonster.exceptions.CinemaDeleteExeption;
import com.example.Cinemonster.exceptions.CinemaNotFoundException;
import com.example.Cinemonster.exceptions.CinemasNotFoundException;
import com.example.Cinemonster.repositories.CinemaRepository;
import com.example.Cinemonster.enums.ExceptionReasons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CinemaService {

    @Autowired
    private CinemaRepository cinemaRepository;

    public ResponseCinemaDTO createCinema(RequestCinemaDTO requestCinemaDTO) {
        if (isCinemaDTOCorrect(requestCinemaDTO)) {
            throw new CinemaCreateException("Erstellen des Kinos hat nicht funktioniert", ExceptionReasons.CINEMA_NOT_FOUND);
        }
        CinemaEntity cinema = new CinemaEntity();
        cinema.setName(requestCinemaDTO.getName());
        cinema.setAddress(requestCinemaDTO.getAddress());
        cinema.setManager(requestCinemaDTO.getManager());
        cinema.setMaxHalls(requestCinemaDTO.getMaxHalls());
        cinemaRepository.save(cinema);
        return ResponseCinemaDTO.builder().name(cinema.getName()).address(cinema.getAddress()).manager(cinema.getManager()).maxHalls(cinema.getMaxHalls()).build();
    }

    public ResponseCinemaDTO findCinemaById(int cinemaId) {
        Optional<CinemaEntity> cinemaEntityOptional = cinemaRepository.findById(cinemaId);
        if (cinemaEntityOptional.isEmpty()) {
            throw new CinemaNotFoundException("Cinema Not Found: " + cinemaId, ExceptionReasons.CINEMA_NOT_FOUND);
        }
        CinemaEntity cinema = cinemaEntityOptional.get();
        List<Integer> cleanList = new ArrayList<>();
        return convertToDTO(cinema, cleanList);
    }

    private ResponseCinemaDTO convertToDTO(CinemaEntity cinema, List<Integer> cinemaList) {
        ResponseCinemaDTO responseCinemaDTO = new ResponseCinemaDTO();
        responseCinemaDTO.setName(cinema.getName());
        responseCinemaDTO.setAddress(cinema.getAddress());
        responseCinemaDTO.setManager(cinema.getManager());
        responseCinemaDTO.setMaxHalls(cinema.getMaxHalls());
        responseCinemaDTO.setGetCinemaHalls(cinemaList);
        return responseCinemaDTO;
    }

    public List<ResponseCinemaDTO> getAllCinemas() {
        List<CinemaEntity> cinemas = cinemaRepository.findAll();
        if (cinemas.isEmpty()) {
            throw new CinemasNotFoundException("Keine Kinos vorhanden");
        }
        List<ResponseCinemaDTO> requestCinemaDTOS = new ArrayList<>();
        for (CinemaEntity cinema : cinemas) {
            List<Integer> cinemaList = new ArrayList<>();
            for (HallEntity hallEntity : cinema.getHalls()) {
                int hallId = hallEntity.getHallId();
                cinemaList.add(hallId);
            }
            requestCinemaDTOS.add(convertToDTO(cinema, cinemaList));
        }
        return requestCinemaDTOS;
    }

    public String deleteCinema(int cinemaId) {
        Optional<CinemaEntity> cinemaOptional = cinemaRepository.findById(cinemaId);
        if (cinemaOptional.isEmpty()) {
            throw new CinemaNotFoundException("Kino kann nicht gelöscht werden", ExceptionReasons.CINEMA_NOT_FOUND);
        }
        CinemaEntity cinema = cinemaOptional.get();
        List<HallEntity> halls = cinema.getHalls();
        for (HallEntity hall : halls) {
            List<MovieEntity> movies = hall.getMovies();
            if (!movies.isEmpty()) {
                throw new CinemaDeleteExeption("Kino kann nicht gelöscht werden, da noch Filme zugeordnet sind");
            }
        }
        cinemaRepository.delete(cinema);
        return "Kino wurde gelöscht";
    }

    private boolean isCinemaDTOCorrect(RequestCinemaDTO requestCinemaDTO) {
        return requestCinemaDTO.getName().isEmpty() || requestCinemaDTO.getAddress().isEmpty() || requestCinemaDTO.getManager().isEmpty() || requestCinemaDTO.getMaxHalls() < 1;
    }
}