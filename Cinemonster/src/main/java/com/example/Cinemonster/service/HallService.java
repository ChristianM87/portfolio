package com.example.Cinemonster.service;


import com.example.Cinemonster.dto.request.RequestHallDTO;
import com.example.Cinemonster.dto.response.ResponseHallDTO;
import com.example.Cinemonster.entities.CinemaEntity;
import com.example.Cinemonster.entities.HallEntity;
import com.example.Cinemonster.enums.MovieVersion;
import com.example.Cinemonster.exceptions.HallCreateException;
import com.example.Cinemonster.exceptions.MovieVersionException;
import com.example.Cinemonster.repositories.CinemaRepository;
import com.example.Cinemonster.repositories.HallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;

@Service
public class HallService {

    @Autowired
    HallRepository hallRepository;

    @Autowired
    CinemaRepository cinemaRepository;

    public ResponseHallDTO createHall(RequestHallDTO requestHallDTO) {
        if (requestHallDTO.getOccupiedSeats() > requestHallDTO.getCapacity()) {
            throw new HallCreateException("Erstellen des Saals hat nicht funktioniert");
        }
        HallEntity hall = new HallEntity();
        hall.setCapacity(requestHallDTO.getCapacity());
        hall.setOccupiedSeats(requestHallDTO.getOccupiedSeats());
        hall.setSupportedMovieVersion(requestHallDTO.getSupportedMovieVersion());
        Optional<CinemaEntity> cinemaEntityOptional = cinemaRepository.findById(requestHallDTO.getCinemaId());
        CinemaEntity cinemaEntity = cinemaEntityOptional.get();

        hall.setCinemaEntity(cinemaEntity);

        int numberOfHallsForCinema = hallRepository.countByCinemaEntity(cinemaEntity);
        if (numberOfHallsForCinema >= 3) {
            throw new HallCreateException("Es gibt bereits zu viele Sääle in diesem Kino");
        }
        hallRepository.save(hall);
        return new ResponseHallDTO(hall.getHallId(), hall.getCapacity(), hall.getOccupiedSeats(), hall.getSupportedMovieVersion(), hall.getCinemaEntity().getCinemaId());
    }

    public ResponseHallDTO updateHall(int hallId, RequestHallDTO requestHallDTO) {
        Optional<HallEntity> hallOptional = hallRepository.findById(hallId);
        HallEntity hall = hallOptional.get();
        hall.setCapacity(requestHallDTO.getCapacity());
        hall.setOccupiedSeats(requestHallDTO.getOccupiedSeats());
        MovieVersion oldVersion = hall.getSupportedMovieVersion();
        MovieVersion newVersion = requestHallDTO.getSupportedMovieVersion();
        if ((oldVersion == MovieVersion.D2D && newVersion != MovieVersion.D2D) ||
                (oldVersion == MovieVersion.R3D && newVersion != MovieVersion.R3D) ||
                (oldVersion == MovieVersion.DBOX && newVersion == MovieVersion.D2D)){
            throw new MovieVersionException("MovieVersion darf nur von 5D auf 3D geändert werden");
        }
        hall.setSupportedMovieVersion(requestHallDTO.getSupportedMovieVersion());
        hallRepository.save(hall);
        return new ResponseHallDTO(hall.getHallId(), hall.getCapacity(), hall.getOccupiedSeats(), hall.getSupportedMovieVersion(), hall.getCinemaEntity().getCinemaId());
    }

    public ResponseHallDTO showHall(int hallId){
        Optional<HallEntity> hallOptional = hallRepository.findById(hallId);
        HallEntity hall = hallOptional.get();
        hall.getHallId();
        hall.getCapacity();
        hall.getOccupiedSeats();
        hall.getSupportedMovieVersion();
        return new ResponseHallDTO(hall.getHallId(), hall.getCapacity(), hall.getOccupiedSeats(),hall.getSupportedMovieVersion(), hall.getCinemaEntity().getCinemaId());
    }
}



