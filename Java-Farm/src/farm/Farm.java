package farm;

import animals.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Farm {
    private Animal lastFed;
    private boolean isAnimalDead = false;
    private int collectedWool = 0;
    private int collectedMilk = 0;
    private int collectedEggs = 0;
    private int collectedTeeth = 0;

    List<Chicken> listChicken = new ArrayList<>();
    List<Cow> listCow = new ArrayList<>();
    List<Sheep> listSheep = new ArrayList<>();
    List<Megalodon> listMegalodon = new ArrayList<>();

    List<Animal> allAnimals = new ArrayList<>();


    public void addSheep(String newName, int newAge, int newHunger) {
        Sheep sheep = new Sheep(newName, newAge, newHunger, 0);
        listSheep.add(sheep);
        allAnimals.add(sheep);
    }

    public void addCow(String newName, int newAge, int newHunger) {
        Cow cow = new Cow(newName, newAge, newHunger, 0);
        listCow.add(cow);
        allAnimals.add(cow);
    }

    public void addChicken(String newName, int newAge, int newHunger) {
        Chicken chicken = new Chicken(newName, newAge, newHunger, 0);
        listChicken.add(chicken);
        allAnimals.add(chicken);
    }

    public void addMegalodon(String newName, int newAge, int newHunger) {
        Megalodon megalodon= new Megalodon(newName, newAge, newHunger, 0);
        listMegalodon.add(megalodon);
        allAnimals.add(megalodon);
    }

    public void userInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Möchten sie ein Huhn/Schaf/Kuh/Meg hinzufügen zur Farm?");
        String tierArt = scanner.next();
        if (tierArt.equals("Huhn") || tierArt.equals("Schaf") || tierArt.equals("Kuh") || tierArt.equals("Meg")) {
            System.out.println("Wie soll das Tier heißen?");
            String name = scanner.next();
            System.out.println("Wie alt soll das Tier sein?");
            int alter = scanner.nextInt();
            System.out.println("Wieviel Hunger hat das Tier?");
            int hunger = scanner.nextInt();

            switch (tierArt) {
                case "Huhn" -> addChicken(name, alter, hunger);
                case "Schaf" -> addSheep(name, alter, hunger);
                case "Kuh" -> addCow(name, alter, hunger);
                case "Meg" -> addMegalodon(name, alter, hunger);
            }
        } else if (tierArt.equals("nein".toLowerCase())) {
            System.out.println("Es wird kein Tier hinzugefügt");
        }
    }

    public void printAnimals() {
        if (!listChicken.isEmpty()) {
            System.out.println("Hühner:");
            for (Chicken chicken : listChicken) {
                System.out.println("Name: " + chicken.getName() + ", Alter: " + chicken.getAge() + ", Hunger: " + chicken.getHunger());
            }
        }
        if (!listSheep.isEmpty()) {
            System.out.println("Schafe:");
            for (Sheep sheep : listSheep) {
                System.out.println("Name: " + sheep.getName() + ", Alter: " + sheep.getAge() + ", Hunger: " + sheep.getHunger());
            }
        }
        if (!listCow.isEmpty()) {
            System.out.println("Kühe");
            for (Cow cow : listCow) {
                System.out.println("Name: " + cow.getName() + ", Alter: " + cow.getAge() + ", Hunger: " + cow.getHunger());
            }
        }
        if (!listMegalodon.isEmpty()) {
            System.out.println("Meg");
            for (Megalodon megalodon : listMegalodon) {
                System.out.println("Name: " + megalodon.getName() + ", Alter: " + megalodon.getAge() + ", Hunger: " + megalodon.getHunger());
            }
        }
        medication();
    }

    public void feedAnimal() {
        printAnimals();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sag mir den Namen des Tieres, das gefüttert werden soll!");
        String name = scanner.next();
        Animal animalFeed = findAnimal(name);
        if (animalFeed != null) {
            animalFeed.feed();
            lastFed = animalFeed;
        } else {
            System.out.println("Tier wurde nicht gefunden");
        }
    }

    public Animal findAnimal(String name) {
        for (Animal animal : allAnimals) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }


    public void starve() {
        for (Animal animal : allAnimals) {
            if (animal != lastFed) {
                animal.starve();
            }
            if (animal.getHunger() > 100) {
                if(animal instanceof Megalodon && allAnimals.size() > 1){
                    animal.eat();
                    System.out.println("Der Megalodon hat das langsamste Tier verschlungen. Der Farmer ist traurig, und verkauft seine Farm");
                }
                isAnimalDead = true;
            }
        }
        lastFed = null;
    }

    public void ageAnimals() {
        for (Animal animal : allAnimals) {
            animal.setAge(animal.getAge() + 1);
        }
    }

    public boolean isAnimalDead() {
        return isAnimalDead;
    }

    public void collectResources() {
        for (Animal animal : listSheep) {
            collectedWool += animal.collectResources();
        }
        for (Animal animal : listChicken) {
            collectedEggs += animal.collectResources();
        }
        for (Animal animal : listCow) {
            collectedMilk += animal.collectResources();
        }
        for (Animal animal : listMegalodon) {
            collectedTeeth += animal.collectResources();
        }
    }

    public void printResources(){
        System.out.println("Wolle: " + collectedWool + " Eier: " + collectedEggs + " Milch: " + collectedMilk + " Zähne " + collectedTeeth);
    }

    public void medication(){
        for (Animal animal : allAnimals){
            if (animal.getAge() >= 20){
                System.out.println("Leqembi wurde " + animal.getName() + " verabreicht");
            }
        }
    }
}

