package animals;

public class Sheep extends Animal{
    private int wool;

    public Sheep(String name, int age, int hunger, int wool) {
        super(name, age, hunger);
        this.wool = wool;
    }

    @Override
    public int collectResources() {
        int collectedWool = wool;
        wool = 0;
        return collectedWool;
    }


    @Override
    public void eat() {
        wool++;
        hunger -= 20;
    }
}
