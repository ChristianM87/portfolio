package animals;

public class Cow extends Animal{
    private int milk;


    public Cow(String name, int age, int hunger, int milk){
        super(name, age, hunger);
        this.milk = milk;
    }

    @Override
    public int collectResources() {
        int collectedMilk = milk;
        milk = 0;
        return collectedMilk;
    }

    @Override
    public void eat() {
        milk++;
        hunger -= 20;
    }
}
