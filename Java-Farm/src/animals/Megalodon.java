package animals;

public class Megalodon extends Animal{

    private int teeth;
    public Megalodon(String name, int age, int hunger, int teeth) {
        super(name, age, hunger);
        this.teeth = teeth;
    }

    @Override
    public int collectResources() {
        int collectedTeeth = teeth;
        teeth = 0;
        return collectedTeeth;
    }

    @Override
    public void eat() {
        teeth++;
        hunger -= 20;
    }

    @Override
    public void starve(){
        hunger += 30;
    }


}
