package animals;

public abstract class Animal implements HungerManager {
    protected String name;
    protected int hunger;
    protected int age;


    public Animal(String name, int age, int hunger){
        this.name = name;
        this.age = age;
        this.hunger = hunger;
    }


    public abstract int collectResources();

    public int feed(){
        eat();
        return hunger;
    }


    public String getName() {
        return name;
    }

    public int getHunger() {
        return hunger;
    }

   public abstract void eat();

   public void starve(){
       hunger += 20;
   }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

}

