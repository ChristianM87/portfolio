package animals;

public class Chicken extends Animal{
    private int eggs;


    public Chicken(String name, int age, int hunger, int eggs){
        super(name, age, hunger);
        this.eggs = eggs;
    }

    @Override
    public int collectResources() {
        int collectedEggs = eggs;
        eggs = 0;
        return collectedEggs;
    }


    @Override
    public void eat() {
        eggs++;
        hunger -= 20;
    }
}
