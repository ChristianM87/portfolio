import farm.Farm;

public class Main {


    public static void main(String[] args) {
        System.out.println("Herzlich Willkommen zu Ihrer Farm Crazy Animals.");
        System.out.println("Spielregeln");
        System.out.println("Fügen Sie Schafe/Kühe/Hühner/Megs zu Ihrer Farm hinzu, und lassen Sie Ihre Farm wachsen");
        System.out.println("Aber vorsicht, lassen Sie Ihre Tiere nicht verhungern oder zu hungrig werden!!!");
        Farm farm = new Farm();
        int count = 0;

        while (!farm.isAnimalDead()) {
            farm.userInput();
            farm.collectResources();
            farm.printResources();
            farm.feedAnimal();
            farm.starve();
            count++;
            if (count % 3 == 0) {
                farm.ageAnimals();
            }
        }
    }
}


