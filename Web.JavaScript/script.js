const personTableBody = document.getElementById('personTableBody');
const sortTableButtons = Array.from(document.getElementsByClassName('sort-table'));
const printAllPersonsButton = document.getElementById('printAllPersons');

let persons = [
    createPersonObject('Maria', 'Huber', 1990),
    createPersonObject('Franz', 'Müller', 1978),
    createPersonObject('Gerhard', 'Gruber', 1991),
    createPersonObject('Alina', 'Steiner', 1997)
];
render();


function createPersonObject(firstName, lastName, birthYear) {
    return {
        firstName,
        lastName,
        birthYear,
        get age() {
            const currentYear = new Date().getFullYear();
            return currentYear - this.birthYear;
        },
        set age(age) {
            const currentYear = new Date().getFullYear();
            const newBirthYear = currentYear - age;
            this.birthYear = newBirthYear;
        }
    };
    render();
}

personTableBody.addEventListener('click', event => {
    if (event.target.classList.contains('delete-person')) {
        const firstName = event.target.dataset.firstName;
        const lastName = event.target.dataset.lastName;
        const indexToDelete = persons.findIndex(person => person.firstName === firstName && person.lastName === lastName);

        if (indexToDelete !== -1) {
            const confirmationMessage = `Sind Sie sicher, dass ${firstName} ${lastName} gelöscht werden soll?`;
            if (confirm(confirmationMessage)) {
                persons.splice(indexToDelete, 1);
                render();
            }
        }
    }
});

sortTableButtons.forEach(button => {
    button.addEventListener('click', () => {
        const sortKey = button.dataset.sortKey;
        sortPersons(sortKey);
        render();
    });
});

function sortPersons(key) {
    persons.sort((a, b) => {
        if (a[key] < b[key]) return -1;
        if (a[key] > b[key]) return 1;
        return 0;
    });
}

printAllPersonsButton.addEventListener('click', showAllPersons);

function showAllPersons() {
    const personStrings = persons.map(person => `${person.firstName} ${person.lastName} - Alter: ${person.age}, Geburtsjahr: ${person.birthYear}`);
    const allPersonsAlert = personStrings.join('\r\n');
    alert(allPersonsAlert);
}


    const addPersonForm = document.getElementById('addPersonForm');
    const addPersonFirstName = document.getElementById('addPersonFirstName');
    const addPersonLastName = document.getElementById('addPersonLastName');
    const addPersonBirthYear = document.getElementById('addPersonBirthYear');
    const clearAllButton = document.getElementById('clearAllButton');

    addPersonForm.addEventListener('submit', event => {
        event.preventDefault();

        const firstName = addPersonFirstName.value;
        const lastName = addPersonLastName.value;
        const birthYear = parseInt(addPersonBirthYear.value);

        persons.push(createPersonObject(firstName, lastName, birthYear));

        addPersonFirstName.value = '';
        addPersonLastName.value = '';
        addPersonBirthYear.value = '';

        render();
    });

    clearAllButton.addEventListener('click', () => {
        persons = [];
        render();
    });

    personTableBody.addEventListener('click', event => {
        if (event.target.classList.contains('update-age')) {
            let age = NaN;
            for (let i = 0; i < 3 && isNaN(age); i++) {
                age = parseInt(prompt('Geben Sie das neue Alter an'));
            }
            if (!isNaN(age)) {
                const personIndex = parseInt(event.target.dataset.index);
                persons[personIndex].age = age;
            }
        }
        render();
    });

    function render() {
        personTableBody.innerHTML = '';
        persons.forEach((person, index) => {
            let deletePersonButton = `<button type="button" class="btn btn-danger btn-sm delete-person" data-first-name="${person.firstName}" data-last-name="${person.lastName}">`;
            deletePersonButton += 'Löschen';
            deletePersonButton += '</button>';

            let updatePersonButton = `<button type="button" class="btn btn-warning btn-sm update-age" data-index="${index}">`;
            updatePersonButton += 'Alter ändern'
            updatePersonButton += '</button>'

            let personTableRow = '<tr>';
            personTableRow += `<td>${person.firstName}</td>`;
            personTableRow += `<td>${person.lastName}</td>`;
            personTableRow += `<td>${person.age} (Geburtsjahr: ${person.birthYear})</td>`;
            personTableRow += `<td class="text-end">${updatePersonButton} ${deletePersonButton}</td>`;
            personTableRow += '</tr>';
            personTableBody.innerHTML += personTableRow;
        });

}